require "fseq4r/version"
require 'yaml'
require 'csv'

# Ruby module to manage F-sequences.
#
# This module has many functions implemented about F-sequence processes.
# Here we have implemented first and second transformation primitives
# algorithms, two search algorithms: pt1 and pt2, respectively, and other two
# compute those transformations: find_pt1 and find_pt2.
#
# You can find theorical documentation and matematical basis about this
# library in this {https://israelbuitron.box.com/s/d9qyce5mhl1isnpd5so4iqmifbefxb49 PDF} file.
#
# @author Israel Buitrón ({mailto:ibuitron@ipn.mx email},
#   {http://comunidad.escom.ipn.mx/ibuitron webpage})
module Fseq4r
  TAU_1 = 0x1
  TAU_2 = 0x2
  TAU_3 = 0x3



  # Given a sequence of symbols, validates if constains only symbols
  # in set \\{1,...,n\}.
  #
  # @example
  #   has_valid_symbols?([1,2,1,2],3) # true
  #   has_valid_symbols?([1,2,1,3],3) # true
  #   has_valid_symbols?([1,2,1,4],3) # false
  #   has_valid_symbols?([0,2,1,3],3) # false
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] n An integer equals or greater than 3.
  #
  # @return [Boolean] Returns true when sequence has only valid
  #   symbols, otherwise false.
  #
  # @raise [ArgumentError] When these cases:
  #   - n is not an integer equals o greater than 3.
  #   - seq is empty.
  def self.has_valid_symbols?(seq,n)
    raise ArgumentError, 'n must be an integer equals or greater than 3' unless n>=3
    raise ArgumentError, 'seq must not be empty' if seq.empty?

    # Must have elements only in {1,...,n} set.
    seq.each { |s| return false unless (1..n).include?(s)}

    true
  end



  # Given a F-sequence, it search all odd-parity symbols.
  #
  # @param [Array] fseq An F-sequence.
  # @return [Array] A new array with odd-parity symbols, if F-sequence is empty
  #   then it will return an empty set.
  def self.parity(fseq)
    # If sequence is empty
    return [] if fseq.empty?

    # Duplicate sequence
    temp = Array.new(fseq)

    # If sequence size is 1
    return temp if fseq.size == 1

    # If sequence size is greater than 1
    odd_partity = []

    while !temp.empty?
      a = temp.first
      c = temp.count(a)
      odd_partity << a if c%2==1
      temp.delete(a)
    end

    odd_partity
  end



  # Computes available numbers for an i-th position in a sequences.
  #
  # For this search problem there are four cases, when search is requested
  # for:
  # - first position in sequence,
  # - second position in sequence,
  # - last position in sequence,
  # - other position (between third and previous to last position) in sequence
  #
  # @param [Array<Integer>] seq An sequence of integer numbers in set \\{1,...,n\}.
  # @param [Integer] n Order of sequence. An integer number greater or equals
  #   to 3.
  # @param [Integer] i Position of sequence to compute posible numbers. An
  #   integer number between 0 (inclusive) and 2^n (exclusive).
  # @return [Array<Integer>] An array of integer numbers between 1 and n
  #   (both inclusive) which are valid numbers to be set in i-th position of
  #   sequence.
  def self.search_item_for_index(seq,n,i)
    # Avoid i<0 and i>2^n-1
    raise ArgumentError, 'Index i must be between zero and 2^n-1' if i<0 || i>2**n-1

    avai = (1..n).to_a
    parity = 0

    # First index has all numbers available
    return avai if i==0

    # Second index has all numbers available but first
    return avai-[seq.first] if i==1

    # Last index must be unique odd-parity number
    if i==2**n-1
      # Toogle bits to check parity of items in sequence
      (0...i).each { |j| parity ^= (1<<(seq[j]-1)) }

      # When there are not an unique number with odd-parity, there is not
      # numbers for last position in sequence
      bits = parity.to_s(2)
      return [] unless bits.count("1")==1

      # Check for unique bit set in parity flag (not-unique is an error).
      l=0
      l+=1 until (parity >> l)==1

      return [l+1]
    end

    # Other indices except first, second and last
    allow = []

    # Discar (i-1)-th number in secuence as available
    avai.delete(seq[i-1])

    # For each number available validate
    avai.each do |m|
      # Validate parity for subsequence staring in index j
      (0...i).each do |j|
        # Clean parity flag when evaluate new subsequence
        parity = 0

        # Compute parity for subsequence
        (j...i).each { |k|  parity ^= 1<<(seq[k]-1) }
        parity ^= (1<<(m-1))

        # Stop because subsequence [j,...,i-1]+[m] has even-parity
        break if parity==0
      end

      # When no subsequences have even-parity, m is valid for i-th position
      allow << m if parity!=0
    end

    allow
  end



  # Given a sequence of symbols, it validates if is a well-formed f-sequence.
  #
  # @example
  #   is_fsequence?([1,2,1,3,1,2,1,3],3)                 # true
  #   is_fsequence?([1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4],4) # true
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] n An integer equals or greater than 3.
  # @param [Integer] verbose A boolean value. When true and some proper
  #   subsequence results as not well-formed in validation, then will print
  #   in STDERR this subsequence.
  #
  # @return [Boolean] If sequence is a valid f-sequence returns true, otherwise false.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.is_fsequence?(seq,n,verbose=false)
    raise ArgumentError, 'n must be an integer equals or greater than 3' if !n.is_a?(Integer) || n<3

    # Must have 2^n elements
    return false unless seq.size == 2**n

    # Must have elements only in {1,...,n} set.
    return false unless has_valid_symbols?(seq,n)

    # Must have odd-parity elements for each proper subsequence.
    (0..(2**n - 2)).each do |i|
      ((i+1)..(2**n - 1)).each do |j|
        # Break when indices are out of array
        break if j-i >= 2**n - 1

        # Validate odd-parity
        if parity(seq[i..j]).empty?
          STDERR.puts "#{seq[i..j].inspect}"
          return false
        end
      end
      next
    end

    # It is a valid fsequence
    true
  end



  # Given a csv filename, it validates for each sequence if it is
  # a well-formed f-sequence.
  #
  # @param [String] csv_filename A path to CSV file.
  # @param [Integer] n An integer equals or greater than 3.
  # @param [Boolean] break_on_fail A flag to break search at first
  #   non-valid f-sequence. If false then will not stop until search
  #   all sequences in file.
  #
  # @return [Boolean] If all sequences in CSV file are valid
  #   f-sequences returns true, otherwise false.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.are_fsequences_from_csv?(csv_filename, n, break_on_fail=true)
    raise ArgumentError, 'n must be an integer equals or greater than 3' if !n.is_a?(Integer) || n<3

    valid = true

    CSV.foreach(csv_filename, converters: :integer) do |row|
      unless is_fsequence? row,n
        STDERR.puts({n: n, seq: row}.inspect)
        valid = false
        return valid if break_on_fail
      end
    end

    valid
  end



  # Computes tau_2 function given a sequence.
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] i A positive integer to circular-rotate input
  #   sequence.
  #
  # @return [Array] A rotated sequence.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.tau2(seq,i)
    raise ArgumentError, 'i must be a positive integer' if !i.is_a?(Integer) || i<0

    # When i > n, then i mod n
    i = i%seq.size

    seq[-i..-1]+seq[0...-i]
  end



  # Computes tau_3 function given a sequence and its order.
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] n An integer equals or greater than 3.
  #
  # @return [Array] A sequence where its result of apply tau_3 to
  #   input sequence.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.tau3(seq,n)
    raise ArgumentError, 'n must be an integer equals or greater than 3' if !n.is_a?(Integer) || n<3

    seq.map { |s| n-s+1 }
  end



  # Computes tau^p function given a sequence and its order.
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] n An integer equals or greater than 3.
  #
  # @return [Array] A sequence where its result of apply tau_3 to
  #   input sequence.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.tauP(n)
    # Clear variables
    perm,i = [],0

    # Create PRNG
    prng = Random.new()

    # Fill permutation
    while perm.size < n
      perm << i unless perm.include?(i = prng.rand(n))
    end

    # Standarize numbers in 1..n range
    perm.map! { |p| p+1 }

    perm
  end



  def self.apply_perm(seq,n,perm)
    raise ArgumentError, 'n must be an integer equals or greater than 3' if !n.is_a?(Integer) || n<3

    seq.map { |s| perm[s-1] }
  end



  # Computes tau_4 function given a sequence and its order.
  #
  # @param [Array] seq A sequence of symbols.
  # @param [Integer] n An integer equals or greater than 3.
  #
  # @return [Array] A sequence where its result of apply tau_4 to
  #   input sequence.
  #
  # @raise [ArgumentError] If
  #   - n is not an integer equals o greater than 3.
  def self.tau4(seq,n)
    raise ArgumentError, 'n must be an integer equals or greater than 3' if !n.is_a?(Integer) || n<3

    perm = tauP(n)
    seq.map { |s| perm[s-1] }
  end



  def self.eval(t=[])
    result = t.first[1][0]

    # For each tranformation queued, apply it!
    t.each_with_index do |transf,index|
      # Transformation identifier (tau)
      case transf[0]

      when TAU_1
        result = result.reverse

      when TAU_2
        shift = transf[1][2]
        result = tau2(result,shift)

      when TAU_3
        rotation = transf[1][1]
        result = tau3(result,rotation)

      end
    end

    result
  end



  def self.load_t_from_yaml(file)
    raise ArgumentError, "File does not exist" unless File.file?(file)

    YAML.load_file(file)
  end




  # Computes a Gray code an n order.
  #
  # @example
  #   gen_gray_code(1) # [0,1]
  #   gen_gray_code(2) # [0,2,3,1]
  #   gen_gray_code(3) # [0,4,6,2,3,7,5,1]
  #
  # @param n [Integer] Gray code order, it must be a positive integer.
  #
  # @return [Array] A sequence with Gray code of order n.
  #
  # @raise [ArgumentError] If n is not a positive integer.
  def self.gen_gray_code(n)
    raise ArgumentError, "n must be positive integer" if n<=0 || !n.is_a?(Integer)

    two_to_n   = 1 << n
    two_to_n_1 = 1 << (n-1)
    x_n        = Array.new(two_to_n)

    if n==1
      [0,1]
    else
      n_prev = gen_gray_code(n-1)
      n_prev_r = n_prev.reverse

      # Fill first half: X_{n-1}0
      (0...two_to_n_1).each { |i| x_n[i] = n_prev[i] << 1 }
      # Fill second half: reverse(X_{n-1})1
      (two_to_n_1...two_to_n).each { |i| x_n[i] = (n_prev_r[i-two_to_n_1] << 1) + 1 }

      x_n
    end
  end

  # Computes a sequence of indices to generate a F-sequence.
  #
  #    D_1 = [1]
  #    D_n = D_{n-1} || [n] || D_{n-1}
  #
  # @example
  #   indices_seq(1) # [1]
  #   indices_seq(2) # [1,2,1]
  #   indices_seq(3) # [1,2,1,3,1,2,1]
  #
  # @param n [Integer] Gray code order, it must be a positive integer.
  # @return [Array<Integer>] A sequence of positive integers which describe a
  #   F-sequence.
  # @raise [ArgumentError] if n is not a positive integer.
  def self.indices_seq(n)
    raise ArgumentError, "n must be positive integer" if n<=0 || !n.is_a?(Integer)

    # For n=1
    return [1] if n==1

    # For n>1
    d_prev = indices_seq(n-1)
    d_prev + [n] + d_prev
  end

  def self.apply_indices_seq(i, ind_seq)
    seq = [i]

    # Toggle indices in sequence
    ind_seq.each { |j| seq << (seq.last ^ (1<<(j-1))) }
    seq
  end

  # Decide it there is at least one symbol in a sequence which occurs with
  # odd-parity.
  #
  # @example
  #   has_even_parity?([])        # false
  #   has_even_parity?([1])       # true
  #   has_even_parity?([1,2])     # true
  #   has_even_parity?([1,2,1])   # true
  #   has_even_parity?([1,2,1,2]) # false
  #
  # @param seq [Array<Integer>] Sequence of symbols.
  # @return [Boolean] It return true if there is at least one symbol an
  #   odd-number of times, otherwise returns false.
  def self.has_even_parity?(seq)
    return false if seq.empty?
    return true if seq.size == 1

    x = 0            # Most-left point in sequence
    y = seq.size - 1 # Most-right point in sequence

    # Search in subsequences
    (x..y).each do |i|
      (i..y).each do |j|
        return false if parity(seq[i..j]).empty?
      end
    end

    true
  end

  # Returns index of all symbols in sequence such that the current symbol is
  # equals to given symbol.
  #
  # @example
  #   indices(1,[])                      # []
  #   indices(1,[1])                     # [0]
  #   indices(1,[1,2,3,4,1,2,3,1,2,1,1]) # [0,4,7,9,10]
  #
  # @param a [Array<Integer>] Symbol to search.
  # @param seq [Array<Integer>] Sequence of symbols.
  #
  # @return [Array<Integer>] Return an array of indices where symbol a occurs
  #   in given sequence.
  def self.indices(a,seq)
    # When symbols is not in sequence
    return [] if !seq.include?(a)

    # When symbols is at least one time in sequence
    indices = []
    # One-to-one comparison
    seq.each_with_index { |e, i| indices << i if a==e }
    indices
  end

  # Decides one sequence is a rotation of the second one.
  #
  # @example
  #   is_rotation? [],[]          # true
  #   is_rotation? [1],[1]        # true
  #   is_rotation? [1,2],[2,1]    # false
  #   is_rotation? [1,2,1],[1,2]  # false
  #   is_rotation? [],[1]         # false
  #
  # @param seqA [Array<Integer>] Sequence of symbols.
  # @param seqB [Array<Integer>] Sequence of symbols.
  #
  # @return [Boolean] It return true if first sequence is a rotation of the
  #   other, otherwise returns false.
  def self.is_rotation?(seqA,seqB)
    # False if not same size
    return false if seqA.size != seqB.size
    return true  if seqA.empty? || seqB.empty?
    return true  if seqA = seqB

    indxs = indices(seqA.first, seqB[1..-1])
    indxs.each { |i| return true if seqA == seqB.rotate(i) }

    false
  end

  # def self.is_pt1?(seq)
    #
  # end

  # Given a sequence of order n, it searches all first primitive
  # transformations feasible instances.
  #
  # @param fseq [Array<Integer>] A complete F-sequence of order n.
  # @param n [Integer] Gray code order, it must be a positive integer.
  #
  # @return [Array<Array>] An array of feasible transformations.
  #
  # @raise [ArgumentError] if n is not a positive integer or F-sequence is not
  #   complete.
  #
  # @see pt1
  def self.find_pt1s(fseq,n)
    raise ArgumentError, "n must be positive integer" if n<1 || !n.is_a?(Integer)
    raise ArgumentError, "Sequence size must be 2^n - 1" if fseq.size != 2**n-1

    found = []

    (1..n).each do |s|
      indxs = indices(s,fseq)

      # Pairs cannot be found with current symbol
      next if indxs.size < 2

      pairs = indxs.combination(2)

      pairs.each do |i,j|
        arrA = fseq[0...i]
        arrB = fseq[i+1...j]
        arrC = fseq[j+1..-1]
        a    = fseq[i]
        c    = parity([a]+arrB)
        found << [arrA,arrB,arrC,a] if c.size == 1
      end
    end

    found
  end

  # Given a segmented F-sequence, it applies first primitive
  # transformation if it is feasible.
  #
  # First primitive transformation: Let A,B,C are strings of symbols, a is a
  # symbol, f es a complete F-sequence.
  #
  #     f = AaBaC
  #
  # If sequence aB has an unique odd-parity symbol, c, then, g is a complete
  # F-sequence and non F-equivalent to f, where D is B string reversed.
  #
  #     g = AcDcC
  #
  # @param subA [Array<Integer>] The subsequence A of a F-sequence order n.
  # @param subB [Array<Integer>] The subsequence B of a F-sequence order n.
  # @param subC [Array<Integer>] The subsequence C of a F-sequence order n.
  # @param a [Array<Integer>] The symbol a of a F-sequence to be changed.
  #
  # @return [Array<Integer>] A complete F-sequence non F-equivalent.
  #
  # @raise [ArgumentError] if arrB is empty or if parity of aB sequence is
  #   not unique.
  #
  # @see find_pt1
  def self.pt1(subA, subB, subC, a)
    raise ArgumentError, "Array B must be non-empty." if subB.empty?

    # Apply transformation
    aB = [a]+subB
    c  = parity(aB)

    raise ArgumentError, "Must be unique odd-parity symbol in #{aB}" if c.size != 1

    subA + c + subB.reverse + c + subC
  end

  # Given a sequence of order n, it searches all second primitive
  # transformations feasible instances.
  #
  # @param fseq [Array<Integer>] A complete F-sequence of order n.
  # @param n [Integer] Gray code order, it must be a positive integer.
  #
  # @return [Array<Array>] An array of feasible transformations. Each
  #   transformation will be specified only by indices of ai, aj, bi, and bj
  #   symbols. With those indices pts transformation can be computed easily.
  #
  # @raise [ArgumentError] if n is not a positive integer or F-sequence is not
  #   complete.
  #
  # @see pt2
  def self.find_pt2s(fseq,n)
    raise ArgumentError, "n must be positive integer" if n<1 || !n.is_a?(Integer)
    raise ArgumentError, "Sequence size must be 2^n - 1" if fseq.size != 2**n-1

    # Second primitive transformations found
    found = []

    ai_rng = 1...(fseq.size-7)

    # Find index of ai symbol (most-left a symbol)
    ai_rng.each do |ai|

      a_str = fseq[0...ai]

      aj_rng = (ai+4)...(fseq.size-4)
      next if aj_rng.size == 0       # Avoid empty range for Aj

      # Find index of aj symbol (most-right a symbol)
      aj_rng.each do |aj|
        next if fseq[ai] != fseq[aj] # Avoid Ai and Aj not equal

        aBbCa_par = parity(fseq[ai..aj])
        next if aBbCa_par.size != 1  # Avoid aBbCa non unique parity

        bi_rng = (ai+2)...(aj)
        next if bi_rng.size == 0     # Avoid empty range for Bi

        # Find index of bi symbol (most-left b symbol)
        bi_rng.each do |bi|

          bj_rng = (aj+2)...(fseq.size-1)
          next if bj_rng.size == 0   # Avoid empty range for Bj

          b_str = fseq[(ai+1)...(bi)]
          next if b_str.empty?       # Avoid empty string B

          c_str = fseq[(bi+1)...(aj)]
          next if c_str.empty?       # Avoid empty string C

          # Find index of bj symbol (most-right b symbol)
          bj_rng.each do |bj|
            next if fseq[bi] != fseq[bj] # Avoid Bi and Bj not equal

            bCaDb_par = parity(fseq[bi..bj])
            next if bCaDb_par.size != 1  # Avoid bCaDb non unique parity

            d_str = fseq[(aj+1)...(bj)]
            next if d_str.empty?     # Avoid empty string D

            e_str = fseq[(bj+1)..(-1)]
            next if e_str.empty?     # Avoid empty string E

            found << [ai,bi,aj,bj]
          end

        end
      end

    end

    found
  end

  # Given a segmented F-sequence, it applies second primitive
  # transformation if it is feasible.
  #
  # Second primitive transformation: Let A,B,C,D,E are strings of symbols,
  # a and c are a two symbols, f es a complete F-sequence.
  #
  #     f = AaBbCaDbE
  #
  # If sequence aBbCa and bCaDb have an unique odd-parity symbol c and d
  # respectively, then, g is a complete F-sequence and non F-equivalent to f.
  #
  #     g = AcDdCcBdE
  #
  # @param fseq [Array<Integer>] A complete F-sequence.
  # @param ai [Integer] Index of most-left a symbol in F-sequence.
  # @param aj [Integer] Index of most-right a symbol in F-sequence.
  # @param bi [Integer] Index of most-left b symbol in F-sequence.
  # @param bj [Integer] Index of most-right b symbol in F-sequence.
  #
  # @return [Array<Integer>] A complete F-sequence non F-equivalent.
  #
  # @raise [ArgumentError] If parity of aBbCa and bCaDb sequences are not
  #   unique.
  #
  # @see find_pt2
  def self.pt2(fseq, ai, aj, bi, bj)
    # Apply transformation
    aBbCa = fseq[ai..aj]
    bCaDb = fseq[bi..bj]

    c  = parity(aBbCa)
    d  = parity(bCaDb)

    raise ArgumentError, "Must be unique odd-parity symbol in #{aBbCa}" if c.size != 1
    raise ArgumentError, "Must be unique odd-parity symbol in #{aBbCa}" if d.size != 1

    # Compute transformation
    fseq[0...ai] + c + fseq[(aj+1)...bj] + d + fseq[(bi+1)...aj] + c + fseq[(ai+1)...bi] + d + fseq[(bj+1)..-1]
  end



  # Given a sequences count indices ocurrencies.
  #
  # @param seq [Array<Integer>] F-sequence to evaluate.
  # @param n [Integer] Order of F-sequence.
  #
  # @return [Array<Integer>] A array with count of indices in F-sequence.
  def self.count_indices_in_seq(seq,n)
    # Clear indices counter
    counter = Array.new(n,0)
    # Count indices
    seq.each { |i| counter[i-1]+=1 }

    counter
  end



  # Generates randomly a f-sequence.
  #
  # @param n [Integer] Order of F-sequence.
  # @param seed [Integer] Seed to set the initial state of the pseudorandom
  #   number generator. If seed is omitted, the generator is initialized with
  #   Random::new_seed.
  #
  # @return [Array<Integer>] A f-sequence of orden n.
  def self.gen_random_fsequence(n, seed = Random.new_seed)
    prgn = Random.new(seed)  # PseudoRandom Generator Number
    fseq = Array.new(2**n,0) # Fsequence
    avai = []                # Available items for each index in sequence
    last = 0                 # Last item set in sequence
    iter = 0                 # Iteration counter

    until last==2**n
      iter += 1

      # Compute valid numbers for a specific ("last" computed) position
      avai[last] = Fseq4r::search_item_for_index(fseq,n,last) if avai[last].nil?

      # When no numbers for current position
      if avai[last].empty?
        # Remove avai numbers for current position
        avai[last]=nil

        # Remove previous number in sequence from available numbers for previous position
        avai[last-1].delete(fseq[last-1])

        # Backtrack to previous of last
        last -= 1
        next
      end

      # When unique number for current position
      if avai.size==1
        # Set unique number
        fseq[last] = avai[last].first

        # Forward to next position
        last += 1
        next
      end

      # When at least two numbers for current position, pick randomly one
      m = prgn.rand(avai[last].size)
      fseq[last] = avai[last][m]
      # and forward to next position
      last += 1
      next
    end
    fseq
  end



  # Search the minimum lexicographic equivalent f-sequence.
  #
  # Given a f-sequence it search for its minimum equivalent f-sequence.
  # From f-sequence specification it is well-known that the first number of any
  # f-sequence is 1 (one), so it must start with it.
  # Counts the frequency of numbers of input sequence, group those with same
  # frequency and sort them to find permutations which may have the minimum
  # sequence.
  #
  # @param n [Integer] F-sequence order.
  # @param fs [Array<Integer>] F-sequence to minimize.
  #
  # @return [Array<Integer>] The minimum f-sequence equiavalento to input.
  def self.search_min_fs(n,fs)
    # Map numbers in sequence with its frecuency
    freqs = Fseq4r::frequencies(fs,n)

    # Group numbers with same frequency
    grps = Fseq4r::group_by_freqs(freqs)

    # Build feasible permutations
    grps_prms = Fseq4r::permutations_by_groups(grps)

    # Initialize minimum sequences as input one
    minimum = fs

    # Evaluate all feasible permutations
    grps_prms.each do |perm|
      # Apply permutation
      cnd = fs.map { |e|  perm.index(e)+1 }
      # Evaluate permuted and reverse of permuted sequence
      [cnd,cnd.reverse].each do |cec|
        # Get indices with 1 numbers (there may start minimum sequence)
        idxs = cec.each_index.select { |i| cec[i] == 1 }
        idxs.each do |i|
          # Rotate i places
          cec_r = cec.rotate(i)
          # Compare rotated sequence with minimum
          cec_r.each_index do |j|
            # If same number then keep comparing
            next  if cec_r[j] == minimum[j]
            # if greater number then discard it
            break  if cec_r[j] > minimum[j]
            # if less number then new minimum sequence found
            minimum = cec_r
          end
        end
      end
    end
    minimum
  end



  # Generate a (pseudo)randomly a f-sequence of order n.
  #
  # Divide-and-conquer (DAC) algorithm.
  #
  # @param n [Integer] Order of F-sequence.
  # @param prng [Random] Pseudorandom number generator instance.
  #
  # @return [Array<Integer>] A f-sequence of order n.
  def self.dac_gen_rand_fs(n, prng)

    # Compute random f-sequence for Q_2
    return prng.rand(2)==0 ? [1,2,1,2] : [2,1,2,1] if n==2

    # Compute random f-sequence for Q_3
    if n>2
      fss_s = 2
      fss = Array.new(fss_s)
      fss_s.times { |i| fss[i] = dac_gen_rand_fs(n-1, prng) }

      if fss[0][-1] == fss[1][-1]
        # Set n as last item
        fss[0][-1] = fss[1][-1] = n
      else
        # Rotate until last item match
        while fss[0][-1] != fss[1][-1]
          # Select randomly fss to rotate
          wseq = fss[prng.rand(2)]

          # Select randomly left-rotation (<<,+) or right-rotation (>>,-)
          wseq.rotate!(prng.rand(2) == 0 ? 1 : -1)
        end

        # Set n as last item
        fss[0][-1] = fss[1][-1] = n
      end

      # Flat (n-1)-fsequences
      fss.flatten!

      # Randomize sequence
      perm = random_permutation(n,prng)
      fss.map! { |i| i = perm[i-1] }

      return fss
    end
  end



  # Generate (pseudo)randomly a permutation as an array.
  #
  # Given a set of numbers {1,...,n} pick randomly an item and push it into an
  # array which represents a permutation. Permutation array can be read as
  # "i-th index maps to number in i-th index".
  #
  # @param n [Integer] Maximum number in set of integers {1,...,n}.
  # @param prng [Random] Pseudorandom number generator instance.
  #
  # @return [Array<Integer>] An array representing a permutation.
  def self.random_permutation(n, prng)
    # Initialize arrays
    perm, set = [], (1..n).to_a

    # Pick randomly per round
    perm << set.delete_at(prng.rand(set.size)) until set.size == 0

    perm
  end



  # Given an array with frequencies of numbers, computes groups of those
  # numbers with same frequency.
  #
  # Let be an array with length k and items in set of integers, [1,...,n].
  # Let be a second array with length n and for each i-th item its the sum
  # of ocurrences of (i+1) number in the previous array. So, we can say that
  # this second array is an array of frequencies of items in first array of
  # numbers.
  #
  # @example
  #   group_by_freqs([4,2,2])       # [[1],[2,3]]
  #   group_by_freqs([2,4,2])       # [[2],[1,3]]
  #   group_by_freqs([6,4,4,2])     # [[1],[2,3],[4]]
  #   group_by_freqs([6,6,2,2])     # [[1,2],[3,4]]
  #   group_by_freqs([10,2,6,12,2]) # [[4],[1],[3],[2,5]]
  #
  # @param freqs [Array<Integer>] Array with frequencies of numbers.
  #
  # @return [Array<Integer>] Array of groups by frequencies.
  #
  # @since 0.1.2
  def self.group_by_freqs(freqs)
    n = freqs.size

    # Prepare and fill frequencies map
    freq_map = {}
    n.times { |i|  freq_map[i+1] = freqs[i] }

    # Sort implicit items by their frequencies
    srt_freqs = freq_map.keys.sort { |a,b| freq_map[b] <=> freq_map[a] }

    i=0
    grps = []
    while i < n
      grp = [srt_freqs[i]]
      j = i + 1
      while freq_map[srt_freqs[i]] == freq_map[srt_freqs[j]]
        grp << srt_freqs[j]
        j += 1
      end
      grps << grp
      i = j
    end

    grps
  end



  # Count how many times each number appear in sequence.
  #
  # @return [Array<Integer>] Array with frequencies of numbers.
  #
  # @raise [ArgumentError] If some number in sequence is not in [1,...,n] set.
  #
  # @since 0.1.2
  def self.frequencies(seq,n)
    # Initialize array for n items
    freqs = Array.new(n,0)

    seq.each do |e|
      # Validate current item is in valid set of numbers
      raise ArgumentError unless (1..n).include?(e)
      # Increment counter for each item in sequence
      freqs[e-1] += 1
    end

    freqs
  end



  # Compute permutation of numbers given a grouping by number frequencies.
  #
  # @example
  #   permutations_by_groups([[1],[2,3]])         # [[1,2,3],[1,3,2]]
  #   permutations_by_groups([[2],[1,3]])         # [[2,1,3],[2,3,1]]
  #   permutations_by_groups([[1,2],[3]])         # [[1,2,3],[2,1,3]]
  #   permutations_by_groups([[1],[2,3],[4]])     # [[1,2,3,4],[1,3,2,4]]
  #   permutations_by_groups([[4],[1],[3],[2,5]]) # [[4,1,3,2,5],[4,1,3,5,2]]
  #
  # @param grps [Array<Array<Integer>>] Array with groups.
  #
  # @return [Array<Array<Integer>>] Array with permutation of numbers.
  #
  # @since 0.1.2
  def self.permutations_by_groups(grps)
    return grps.first.permutation(grps.first.size).to_a if grps.size == 1

    grps_prms = []
    grps.each { |grp|  grps_prms << grp.permutation(grp.size).to_a }
    grps_prms.reduce { |mem,grp|  mem.product(grp) }.each { |e| e.flatten! }
  end

end
