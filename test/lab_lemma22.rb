require 'optparse'
require 'fseq4r'

options = {}
opt = OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"

  #
  # Specific options
  #
  opts.separator ""
  opts.separator "Specific Options:"

  #
  # F-sequence order
  #
  opts.on('-n N', Integer, 'Order of f-sequence.') do |n|
    options[:n] = n
  end

  #
  # F-sequence
  #
  opts.on('-f FSEQ', Array,
    'The f-sequence. Array of comma-separated positive integers.'
    ) do |s|
    options[:seq] = s.map { |e| e.to_i }
  end

  #
  # Verbose output
  #
  opts.on('-v', '--[no]-verbose', 'Verbose output.') do
    options[:verbose] = true
  end

  #
  # Help message
  #
  opts.on('-h', '--help', 'Show this message.') do
    STDERR.puts opts
    exit
  end

  #
  # Parse command-line arguments
  #
  begin
    opts.parse!

    # Validate command-line arguments
    if options[:n].nil?
      STDERR.puts 'ERROR: F-sequence dimension must be set.'
      STDERR.puts opts
      exit 1
    elsif options[:seq].nil?
      STDERR.puts 'ERROR: F-sequence must be set.'
      STDERR.puts opts
      exit 1
    end

  rescue
    puts opts
    exit 1
  end

end

n    = options[:n]        # F-sequence dimension
seq  = options[:seq]      # Sequence to evaluate
vrb  = options[:verbose]  # Verbose output



valid = true
puts "j\tminA\tminB\tdiff" if vrb
(1..2**n-1).each do |j|
  minA = seq[0..j].min
  minB = seq[0..j-1].min
  diff = minA - minB

  if diff < 0 || diff > 1
  	STDERR.puts "ERROR: Not valid sequence #{seq} at index #{j}."
  	STDERR.puts "#{seq[0..j]}."
  	exit 1
  end
  puts "#{j}\t#{minA}\t#{minB}\t#{diff}" if vrb
end
