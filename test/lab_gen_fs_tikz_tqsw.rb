require 'optparse'
require 'csv'
require 'fseq4r'

#
# Data structures
#
Options = Struct.new(:n, :fseq, :theta, :tikz, :radius, :ham, :edges)
Vertex  = Struct.new(:number, :angle, :label)
Edge    = Struct.new(:v, :u, :label)

opts = Options.new

OptionParser.new do |o|
  o.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"

  #
  # Specific options
  #
  o.separator ''
  o.separator 'Specific Options:'

  #
  # F-sequence order
  #
  o.on('-n', '--order N', Integer,
    'Order of f-sequence.',
    'Positive integer number greater than 3.'
    ) do |n|
    opts.n = n
  end

  #
  # F-sequence
  #
  o.on('-f', '--fseq SEQUENCE', Array,
    'The f-sequence. Array of comma-separated positive integers.'
    ) do |s|
    opts.fseq = s.map { |e| e.to_i }
  end

  #
  # Help message
  #
  o.on('-h', '--help', 'Show this message.') do
    STDERR.puts o
    exit
  end

  #
  # Parse command-line arguments
  #
  is_error = false

  # Parse arguments
  o.parse!

  if opts.n.nil?                    # Validate presence of -n flag
    STDERR.puts 'ERROR: F-sequence order must be set.'
    is_error = true
  elsif opts.n < 3                  # Validate value of -n flag
    STDERR.puts 'ERROR: F-sequence order must be greater than 2.'
    is_error = true
  elsif opts.n.nil?               # Validate presence of -i flag
    STDERR.puts 'ERROR: F-sequence must be set.'
    is_error = true
  end

  if is_error
    STDERR.puts o
    exit 1
  end
end

#
# Set parameters
#
opts.theta  = (360.to_f / (2**opts.n))
opts.radius = 5
opts.ham    = []
opts.edges  = []
puts "Options #{opts.inspect}."

tikz_styles   = ''
tikz_polygons = ''
tikz_edges    = ''
tikz_vertices = ''


#
# Hamiltonian cycle vertices
#
opts.ham << Vertex.new(0, 0, 'v_{0}')
opts.fseq.each do |e|
  v = Vertex.new
  v.number = opts.ham.last.number ^ (1 << e)
  v.angle  = 1.0 * opts.theta * (opts.ham.size)
  v.label  = "v_{#{v.number}}"

  opts.ham << v
end
opts.ham.pop


#
# Tikz styles
#
tikz_styles = %q(
\tikzstyle{q_vertex} = [circle,fill=MaterialBlue100,draw=MaterialBlue900,text=MaterialBlue900];
\tikzstyle{q_edge} = [draw=MaterialBlue900];
\tikzstyle{q_edge_label} = [fill=MaterialGreen100,draw=MaterialGreen900,text=MaterialGreen900,rounded corners,scale=0.8];

\tikzstyle{twsq1} = [thick,draw=MaterialBlue500,fill=MaterialBlue100];
\tikzstyle{twsq2} = [thick,draw=MaterialGreen500,fill=MaterialGreen100];
\tikzstyle{twsq3} = [thick,draw=MaterialYellow500,fill=MaterialYellow100];
\tikzstyle{twsq4} = [thick,draw=MaterialOrange900,fill=MaterialOrange100];
\tikzstyle{twsq5} = [thick,draw=MaterialDeepOrange500,fill=MaterialDeepOrange100];
\tikzstyle{twsq6} = [thick,draw=MaterialRed500,fill=MaterialRed100];
\tikzstyle{twsq7} = [thick,draw=MaterialPink500,fill=MaterialPink100];
\tikzstyle{twsq8} = [thick,draw=MaterialPurple500,fill=MaterialPurple100];
)

#
# Tikz polygons
#
tikz_polygons = %q(% Polygons (twisted squares, straight squares, etc.)
%\draw [twsq1]  (135:5) -- (157.5:5) -- (315:5) -- (292.5:5) -- cycle;

)

#
# Compute hypercube vertices and edges
#
opts.ham.each do |v|
  tikz_vertices << "\\node (v#{v.number}) at (#{v.angle}:#{opts.radius}) [q_vertex] {$#{v.label}$};\n"

  (0...opts.n).each do |i|
    u = v.number ^ (1 << i)

    next if u < v.number

    opts.edges << Edge.new(v.number,u,i)
    # tikz_edges << "\\draw [-,q_edge]  (v#{v.number}) to node[q_edge_label]{$#{i}$}  (v#{u});\n"
  end
end

opts.edges.sort{ |a,b| a.v <=> b.v }.each do |e|
  tikz_edges << "\\draw [-,q_edge]  (v#{e.v}) to node[q_edge_label]{$#{e.label}$} (v#{e.u});\n"
end

#
# Build tikz code
#
opts.tikz = %Q(
\\begin{tikzpicture}
% Tikz styles
#{tikz_styles}
% Tikz polygons
#{tikz_polygons}
% Hypercube vertices
#{tikz_vertices}
% Hypercube edges
#{tikz_edges}
\\end{tikzpicture}
)

puts "Tikz", opts.tikz
