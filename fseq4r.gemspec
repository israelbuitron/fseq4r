# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fseq4r/version'

Gem::Specification.new do |spec|
  spec.name          = "fseq4r"
  spec.version       = Fseq4r::VERSION
  spec.authors       = ["Israel Buitrón"]
  spec.email         = ["ibuitron@computacion.cs.cinvestav.mx"]

  spec.summary       = %q{Ruby library to use fsequences.}
  spec.homepage      = 'https://gitlab.com/israelbuitron/fseq4r'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  # Use "yard" to build full HTML docs.
  spec.metadata['yard.run'] = 'yri'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', "~> 1.11"
  spec.add_development_dependency 'rake', "~> 10.0"
  spec.add_development_dependency 'yard', '0.8.7.6'
  spec.add_development_dependency 'minitest', "~> 5.0"
  spec.add_development_dependency 'redcarpet', "~> 3.4.0"
end
